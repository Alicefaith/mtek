package ke.co.alicefaith.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class login extends AppCompatActivity {
    private Button fbbtn;
    private Button twitterbtn;
    private Button gmailbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        twitterbtn = findViewById(R.id.twitterbtn);
        twitterbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opentwitter();
            }
        });


        fbbtn = findViewById(R.id.fbbtn);
        fbbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openfb();
            }
        });

        gmailbtn = findViewById(R.id.gmailbtn);
        gmailbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opengmail();
            }
        });



    }
    public void openfb(){
        Intent intent = new Intent(this,facebook.class);
        startActivity(intent);
    }
    public void opentwitter(){
        Intent intent = new Intent(this,twitter.class);
        startActivity(intent);
    }
    public void opengmail(){
        Intent intent = new Intent(this, gmail.class);
        startActivity(intent);
    }





}
