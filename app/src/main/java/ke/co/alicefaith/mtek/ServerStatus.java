package ke.co.alicefaith.mtek;

public class ServerStatus {
    private String Group;
    private String Artifact;
    private String Version;
    private String Status;

    public String getGroup() {
        return Group;
    }

    public String getArtifact() {
        return Artifact;
    }

    public String getStatus() {
        return Status;
    }

    public String getVersion() {
        return Version;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public void setArtifact(String artifact) {
        Artifact = artifact;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
