package ke.co.alicefaith.mtek;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IPingWebService {
    @GET ("/")
    Call<ServerStatus> getStatus();

}